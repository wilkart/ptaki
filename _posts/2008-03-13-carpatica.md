---
date: 2008-03-13 08:38:30
layout: post
title: Akcja Carpatica
description: Stowarzyszenie i Akcja Carpatica zajmuje się badaniem wędrówek ptaków na terenie Karpat. Prowadzi również działania o charakterze edukacyjnym promujące ochronę ptaków i ich siedlisk.
image: /assets/img/posts/carpatica_380x200.png
optimized_image: /assets/img/posts/carpatica_380x200.webp
category: obozy
tags:
  - obozy
  - stowarzyszenia
author: wilkart
link: https://www.carpatica.org
---
