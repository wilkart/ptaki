---
date: 2011-08-16 12:00:00
layout: post
title: Komitet Ochrony Orłów
description: Stowarzyszenie "Komitet Ochrony Orłów" stawia sobie za cele ochronę zasobów przyrodniczych Rzeczypospolitej Polskiej, bioróżnorodności gatunkowej, siedliskowej i krajobrazowej.
image: /assets/img/posts/koo_380x200.png
optimized_image: /assets/img/posts/koo_380x200.webp
category: stowarzyszenia
tags:
  - stowarzyszenia
author: wilkart
link: http://www.koo.org.pl
---
