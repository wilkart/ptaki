---
date: 2007-09-15 12:26:40
layout: post
title: Obóz ornitologiczny Kaliszany
description: Obóz Ornitologiczny "Kaliszany" zorganizowany jest na terenie Małopolskiego Przełomu Wisły na Lubelszczyźnie. Prace prowadzone są na wyspie w dolinie rzeki Wisły w trakcie jesiennej wędrówki ptaków.
image: /assets/img/posts/kaliszany_380x200.png
optimized_image: /assets/img/posts/kaliszany_380x200.webp
category: obozy
tags:
  - obozy
author: wilkart
link: https://kaliszany.blogspot.com
---
