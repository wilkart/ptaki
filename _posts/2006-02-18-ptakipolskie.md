---
date: 2006-02-18 12:00:00
layout: post
title: Stowarzyszenie Ptaki Polskie
description: Stowarzyszenie jest dobrowolnym, samorządnym zrzeszeniem osób fizycznych i prawnych, działających na rzecz ochrony dziko  żyjących gatunków i ich siedlisk, zachowania dziedzictwa przyrodniczego i kulturowego oraz na rzecz rozwoju zrównoważonego.
image: /assets/img/posts/ptakipolskie_380x200.png
optimized_image: /assets/img/posts/ptakipolskie_380x200.webp
category: stowarzyszenia
tags:
  - stowarzyszenia
author: wilkart
link: https://ptakipolskie.pl/
---
