---
date: 2001-12-04 17:00:53
layout: post
title: Ogólnopolskie Stowarzyszenie Ochrony Ptaków
description: OTOP to organizacja pozarządowa o statusie pożytku publicznego. Zajmuje się ochroną dzikich ptaków i miejsc, w których one żyją. Powadzi działania z dziedziny monitoringu, czynnej ochrony przyrody, kształtowania polityk mających wpływ na środowisko oraz edukacji.
image: /assets/img/posts/otop_380x200.png
optimized_image: /assets/img/posts/otop_380x200.webp
category: stowarzyszenia
tags:
  - stowarzyszenia
author: wilkart
link: https://www.otop.org.pl
---
