---
date: 2009-09-01 00:00:00
layout: post
title: Kompletna lista ptaków świata
description: Najbardziej aktualna i kompletna lista ptaków świata zawierająca wszystkie nowe gatunki ptaków, zmiany systematyczne, polskie i angielskie nazewnictwo oraz statusy ochronne gatunków w Polsce.
image: /assets/img/posts/listaptakow_380x200.png
optimized_image: /assets/img/posts/listaptakow_380x200.webp
category: instytucje
tags:
  - instytucje
  - uczelnie
author: wilkart
link: http://listaptakow.eko.uj.edu.pl
---
