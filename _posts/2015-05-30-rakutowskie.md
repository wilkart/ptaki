---
date: 2015-05-30 12:26:40
layout: post
title: Stacja Obrączkowania Ptaków Rakutowskie
description: Stacja Obrączkowania Ptaków "Rakutowskie" jest formalnie prowadzona przez Sekcję Ornitologiczną Koła Przyrodników Studentów Uniwersytetu Jagiellońskiego i zajmuje się badaniem jesiennej migracji ptaków w ostoi "Błota Rakutowskie".
image: /assets/img/posts/rakutowskie_380x200.png
optimized_image: /assets/img/posts/rakutowskie_380x200.webp
category: obozy
tags:
  - obozy
author: wilkart
link: https://rakutowskie.blogspot.com
---
