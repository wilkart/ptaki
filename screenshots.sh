#!/usr/bin/env bash

## brew install firefox
## npm install -g foxshot
## brew install imagemagick


POSTS=$(ls _posts/*.md)
IMG="assets/img/posts"

for POST in ${POSTS}; do
  echo $POST

  ID=$(echo $POST | awk -F- '{print $4}' | awk -F. '{print $1}')
  URL=$(grep link $POST | awk '{print $2}')

  echo $ID
  echo $URL

  foxshot -d 1520x800 ${URL}
  convert screenshot_1520x800.png -resize 380x200 $IMG/${ID}_380x200.png
  magick $IMG/${ID}_380x200.png -quality 50 -define webp:lossless=true $IMG/${ID}_380x200.webp

  rm -f screenshot_1520x800.png
done
